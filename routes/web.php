<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
  return view('welcome');
});
Route::group(['namespace' => 'Example', 'prefix' => ''], function() {
  Route::get('/exception-example', 'ExampleController@exception');
  Route::any('/request-example', 'ExampleController@requestExample');
  Route::get('/example-data', 'ExampleController@tableData');
  Route::get('/example-delete', 'ExampleController@delete');
  Route::post('/example-add', 'ExampleController@add');
  Route::post('/example-edit', 'ExampleController@edit');
});
