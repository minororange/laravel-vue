<?php

namespace App\Models\Example;

use App\Models\BaseModel;

class ExampleModel extends BaseModel
{
  public $table = 'example';

  public $timestamps = false;
}
