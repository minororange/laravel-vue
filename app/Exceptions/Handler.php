<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class Handler extends ExceptionHandler
{
  /**
   * A list of the exception types that are not reported.
   *
   * @var array
   */
  protected $dontReport = [
    //
  ];

  /**
   * A list of the inputs that are never flashed for validation exceptions.
   *
   * @var array
   */
  protected $dontFlash = [
    'password',
    'password_confirmation',
  ];

  /**
   * Report or log an exception.
   *
   * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
   *
   * @param  \Exception $exception
   * @return JsonResponse
   * @throws Exception
   */
  public function report(Exception $exception)
  {
    parent::report($exception);
  }

  /**
   * Render an exception into an HTTP response.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Exception $exception
   * @return JsonResponse|response
   */
  public function render($request, Exception $exception)
  {
    if($exception instanceof ApiException)
    {
      return response()->json(['message' => $exception->getMessage(), 'code' => $exception->getCode()], 500);
    }
    if($exception instanceof ValidationException)
    {
      return $this->handleValidationException($exception);
    }

    return parent::render($request, $exception);
  }


  /**
   * 处理验证类失败异常
   *
   * @param ValidationException $exception
   * @return JsonResponse
   * @date 2018/06/06
   * @author ycz
   */
  public function handleValidationException(ValidationException $exception)
  {
    $errors = $exception->errors();
    array_walk($errors, function(&$error) {
      $error = implode(',', $error);
    });

    return response()->json(['message' => implode(',', $errors), 'code' => 998], 422);
  }
}
