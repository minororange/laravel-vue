<?php
/**
 * Created by PhpStorm.
 * User: ycz
 * Date: 2018/06/06
 * Time: 19:39
 */

namespace App\Exceptions;


class ApiException extends \Exception
{
  public function __construct($message = "", $code = 0)
  {
    parent::__construct($message, $code);
  }
}