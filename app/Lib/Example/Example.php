<?php

namespace App\Lib\Example;

use App\Lib\Base;
use App\Models\Example\ExampleModel;

class Example extends Base
{

  /**
   * 获取列表数据
   *
   * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
   * @date 2018/06/11
   * @author ycz
   */
  public function getListData()
  {
    $data = ExampleModel::query()->orderBy('id','DESC')->paginate();

    return $data->toArray();
  }

  /**
   * 新增
   *
   * @param array $formData
   * @return int 新增条目id
   * @date 2018/06/11
   * @author ycz
   */
  public function add($formData)
  {
    return ExampleModel::query()->insertGetId($formData);
  }

  /**
   * 删除
   *
   * @param int $id
   * @return bool|mixed|null
   * @date 2018/06/11
   * @author ycz
   */
  public function delete($id)
  {
    return ExampleModel::query()->find($id)->delete();
  }

  /**
   * 修改
   *
   * @param int $id
   * @param array $data
   * @return bool|int
   * @date 2018/06/11
   * @author ycz
   */
  public function edit($id, $data)
  {
    return ExampleModel::query()->where(['id' => $id])->update($data);
  }
}