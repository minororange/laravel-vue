<?php

namespace App\Http\Controllers;

class ApiController extends Controller
{
  private $message = '';
  private $code = 0;

  /**
   * @param array $data
   * @return \Illuminate\Http\JsonResponse
   * @date 2018/06/06
   * @author ycz
   */
  public function response(array $data = [])
  {
    $response = [
      'message' => $this->getMessage(),
      'code' => $this->getCode(),
      'data' => $data
    ];

    return response()->json($response);
  }

  /**
   * @return string
   */
  public function getMessage(): string
  {
    return $this->message;
  }

  /**
   * @param string $message
   */
  public function setMessage(string $message)
  {
    $this->message = $message;

    return $this;
  }

  /**
   * @return int
   */
  public function getCode(): int
  {
    return $this->code;
  }

  /**
   * @param int $code
   */
  public function setCode(int $code)
  {
    $this->code = $code;

    return $this;
  }
}
