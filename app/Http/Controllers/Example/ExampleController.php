<?php

namespace App\Http\Controllers\Example;

use App\Exceptions\ApiException;
use App\Http\Controllers\ApiController;
use App\Http\Requests\ExampleRequest;
use App\Lib\Example\Example;
use Illuminate\Support\Facades\Input;

class ExampleController extends ApiController
{
  /**
   * @date 2018/06/06
   * @author ycz
   * @throws ApiException
   */
  public function exception()
  {
    throw new ApiException('Example exception', 1000);
  }

  /**
   * @param ExampleRequest $request
   * @return \Illuminate\Http\JsonResponse
   * @date 2018/06/06
   * @author ycz
   */
  public function requestExample(ExampleRequest $request)
  {
    return $this->setCode(0)->setMessage('request success')->response($request->all());
  }


  /**
   * 列表数据
   *
   * @return \Illuminate\Http\JsonResponse
   * @date 2018/06/11
   * @author ycz
   */
  public function tableData()
  {
    $example = new Example();

    $data = $example->getListData();

    return $this->response($data);
  }

  /**
   * 添加
   *
   * @param ExampleRequest $request
   * @return \Illuminate\Http\JsonResponse
   * @date 2018/06/11
   * @author ycz
   */
  public function add(ExampleRequest $request)
  {
    $example = new Example();
    if($example->add($request->all()))
    {
      return $this->setCode(0)->setMessage('添加成功')->response();
    }

    return $this->setCode(1001)->setMessage('添加失败')->response();
  }

  /**
   * 修改
   *
   * @param ExampleRequest $request
   * @return \Illuminate\Http\JsonResponse
   * @date 2018/06/11
   * @author ycz
   */
  public function edit(ExampleRequest $request)
  {
    $example = new Example();
    $id = $request->post('id');
    if($example->edit($id, $request->all()))
    {
      return $this->setCode(0)->setMessage('修改成功')->response();
    }

    return $this->setCode(1001)->setMessage('修改失败')->response();
  }

  /**
   * 删除
   *
   * @return \Illuminate\Http\JsonResponse
   * @date 2018/06/11
   * @author ycz
   */
  public function delete()
  {
    $example = new Example();
    $id = Input::get('id');
    if($example->delete($id))
    {
      return $this->setCode(0)->setMessage('删除成功')->response();
    }

    return $this->setCode(1001)->setMessage('删除失败')->response();
  }
}
