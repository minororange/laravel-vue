<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ExampleRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $id = $this->request->get('id');
    return [
      'name' => [
        'required',
        Rule::unique('example')->ignore($id),//名字不能重复，排除自己
      ],
      'sex' => 'required',
      'age' => 'required'
    ];
  }

  /**
   * 获取已定义的验证规则的错误消息。
   *
   * @return array
   */
  public function messages()
  {
    return [
      'name.required' => '名字必须填写',
      'name.unique' => '名称重复',
      'sex.required' => '性别必须填写',
      'age.required' => '年龄必须填写'
    ];
  }
}
