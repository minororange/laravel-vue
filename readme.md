# LARAVEL VUE

#### php 版本

> 建议`7.1`

> `php7.0`安全支持到 2018 年 12 月 3 日，`7.1` 则是 2019 年 12 月 1 日，`7.2` 过于激进，可能与部分框架不兼容

> 版本支持详细：[php supported-versions](http://php.net/supported-versions.php)

#### laravel 版本

> 建议`5.5`，LTS 版本。

#### 前端框架 ElementUI + Vue

> ElementUI `2.4` Vue `2.5`

#### 下载安装

> `composer install` 

> `npm install`

> `npm run prod`

> 复制 `.env.example` 重命名为： `.env`

> `php artisan key:generate`

> 将网站域名配置到 `public` 文件夹

#### 注：
1. 具体目录详细可参照 [laravel文档介绍](https://laravel-china.org/docs/laravel/5.5/structure/1284)
2. Laravel 的十八个最佳实践: [laravel项目最佳实践](https://laravel-china.org/articles/12762/eighteen-best-practices-of-laravel)
3. 中文文档：[laravel 5.5中文文档](https://laravel-china.org/docs/laravel/5.5) 、[vue中文文档](https://cn.vuejs.org/v2/guide/)
